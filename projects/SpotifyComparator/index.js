var request = require('request-promise');
var express = require('express');

var client_id = 'f80df6c179394e14a38f311a7b84c33f'; // Your client id
var client_secret = 'd8aea1e9a2e5412487b2441b8cedeb66'; // Your secret



function loginSpotify() {
    var authOptions = {
        method: 'POST',
        url: 'https://accounts.spotify.com/api/token',
        headers: {
            'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
        },
        form: {
            grant_type: 'client_credentials'
        },
        json: true
    };
    return request(authOptions)
        .then(a => a.access_token)
        .catch(e => e);
}

function getSpotifyInfos(params, token) {
    var options = {
        method: 'GET',
        url: 'https://api.spotify.com' + params,
        headers: {
            'Authorization': 'Bearer ' + token
        },
        json: true
    };
    return request(options)
        .then(a => a)
        .catch(e => new Error(e));
}

function getTracks(pId, token) {
    return getSpotifyInfos("/v1/playlists/" + pId + "/tracks", token)
        .then(a => {
            var tracks = [];
            a.items.forEach(e => tracks.push(e.track.name))
            return tracks;
        })
        .catch(e => e);
}

function checkSame(tracks, pName) {
    var checked = {};
    var result = {};

    Object.keys(tracks).forEach((key) => {
        tracks[key].forEach(e => {
            if (!!checked[e]) {
                //console.log(e, "=>",checked[e], "&&", key);
                result[e] = Array.isArray(result[e]) ? result[e].push({id: checked[e], name: pName[checked[e]]}, {id:key, name: pName[key]}) : [{id: checked[e], name: pName[checked[e]]}, {id:key, name: pName[key]}];
            }
            else
                checked[e] = key;
        })
    });
    console.log(Object.keys(checked).length);
    return result;
}

async function checkPlaylist(userId) {
    var token = await loginSpotify()
    var playlists = {};
    var promises = [];
    var pName = {};
    var p = await getSpotifyInfos("/v1/users/" + userId + "/playlists", token)
    p.items.forEach(e => {
        pName[e.id] = e.name;
        promises.push(getTracks(e.id, token).then(a => playlists[e.id] = a));
    });
    return Promise.all(promises)
        .then(() => checkSame(playlists, pName))
        .then(a => {
            var result = [];
            Object.keys(a).forEach((key) => {
                result.push({song:key, list: a[key]});
            });
            return {result}
        });
}

var app = express();
app.use(express.static(__dirname + '/web'));
app.get('/playlist', function(req, res) {
    if (!!req.query.userId) {
        checkPlaylist(req.query.userId).then(a => res.send(a));
    }
});
app.listen(8888);