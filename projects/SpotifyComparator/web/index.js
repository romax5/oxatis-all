function main() {
    fetch('/playlist?userId=11128371515')
        .then(a => {
            a.json()
                .then(a => {
                    var userProfileSource = `{{#each result}}
                    <div class="col-12 col-md-5 shadow rounded p-4 m-4 text-center bg-dark text-white">
                        <h1>{{this.song}}</h1>
                        <hr>
                        <h4>
                            {{#each this.list}}
                            <a target="_blank" href="https://embed.spotify.com/go?uri=spotify:playlist:{{this.this.id}}" class="badge text-white w-100 bg-green py-4 my-2">
                                {{this.this.name}}
                            </a>
                            {{/each}}
                        </h4>
                    </div>
                    {{/each}}`,
                        userProfileTemplate = Handlebars.compile(userProfileSource),
                        userProfilePlaceholder = document.getElementById('spotify-result');
                        userProfilePlaceholder.innerHTML = userProfileTemplate(a);
                        setTimeout(main, 100000);
                })
        })
}

window.addEventListener('DOMContentLoaded', main);