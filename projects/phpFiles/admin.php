<?php
    include("..//header_article.php");
    $pageName = "Admin";
    $noTrack = true;
    if ((isset($_SESSION['perm'])) && ($_SESSION['perm'] >= 10)) {
        setlocale(LC_TIME, 'fr_FR.UTF-8');
        mysql_connect("localhost", "web", "octobre");
        mysql_select_db("MinewhiteV2");
        $query = "SELECT SUM(views) AS VIEWS, COUNT(*) AS NB FROM forum_stats";
        $req = mysql_query($query) or print($erreurText);
        $donnees = mysql_fetch_array($req);
        $totalVisitor = $donnees['NB'];
        $totalViews = $donnees['VIEWS'];

        $today = date("Y-m-d");
        $query2 = "SELECT COUNT(*) AS NB FROM forum_stats WHERE last LIKE '$today %'";
        $req2 = mysql_query($query2) or print($erreurText);
        $donnees2 = mysql_fetch_array($req2);
        $todayMember = $donnees2['NB'];

        $query3 = "SELECT COUNT(*) AS NB FROM forum_membre";
        $req3 = mysql_query($query3) or print($erreurText);
        $donnees3 = mysql_fetch_array($req3);
        $totalMember = $donnees3['NB'];

        $query4 = "SELECT * FROM forum_membre ORDER BY id DESC LIMIT 1";
        $req4 = mysql_query($query4) or print($erreurText);
        $donnees4 = mysql_fetch_array($req4);
        $idLastMember = $donnees4['id'];
        $pseudoLastMember = $donnees4['pseudo'];

        $query5 = "SELECT COUNT(*) AS NB FROM forum_sujet";
        $req5 = mysql_query($query5) or print($erreurText);
        $donnees5 = mysql_fetch_array($req5);
        $totalSubject = $donnees5['NB'];

        $query6 = "SELECT COUNT(*) AS NB FROM forum_message";
        $req6 = mysql_query($query6) or print($erreurText);
        $donnees6 = mysql_fetch_array($req6);
        $totalMessage = $donnees6['NB'];

        $query7 = "SELECT SUM(nbVote) AS NB FROM server_prive";
        $req7 = mysql_query($query7) or print($erreurText);
        $donnees7 = mysql_fetch_array($req7);
        $totalJeton = $donnees7['NB'];

        $query8 = "SELECT SUM(total) AS NB FROM vote";
        $req8 = mysql_query($query8) or print($erreurText);
        $donnees8 = mysql_fetch_array($req8);
        $totalVote = $donnees8['NB'];
        echo '<section class="intro section-padding">
            <div class="container">
                <div class="text-center">
                    <h2>Admin <small><i class="fa fa-angle-double-right" aria-hidden="true"></i> Pannel</small></h2>
                    <div class="col-md-8">
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="circle-tile">
                            <a>
                                <div class="circle-tile-heading purple">
                                    <i class="fa fa-hand-pointer fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content transparent text-purple">
                                <div class="circle-tile-description">Vue totales</div>
                                <div class="circle-tile-number transparent text-purple">'.$totalViews.'</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="circle-tile">
                            <a>
                                <div class="circle-tile-heading purple">
                                    <i class="fa fa-users fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content transparent text-purple">
                                <div class="circle-tile-description">Visiteurs uniques</div>
                                <div class="circle-tile-number transparent text-purple">'.$totalVisitor.'</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="circle-tile">
                            <a>
                                <div class="circle-tile-heading purple">
                                    <i class="fa fa-calendar-alt fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content transparent text-purple">
                                <div class="circle-tile-description">Visiteur journalier</div>
                                <div class="circle-tile-number transparent text-purple">'.$todayMember.'</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="circle-tile">
                            <a>
                                <div class="circle-tile-heading purple">
                                    <i class="fa fa-user-plus fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content transparent text-purple">
                                <div class="circle-tile-description">Membres inscrits</div>
                                <div class="circle-tile-number transparent text-purple">'.$totalMember.'</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="circle-tile">
                            <a>
                                <div class="circle-tile-heading purple">
                                    <i class="fa fa-sign-in-alt fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content transparent text-purple">
                                <div class="circle-tile-description">Dernier inscrit</div>
                                <div class="circle-tile-number transparent text-purple">'.$pseudoLastMember.'</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="circle-tile">
                            <a>
                                <div class="circle-tile-heading purple">
                                    <i class="fa fa-comments fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content transparent text-purple">
                                <div class="circle-tile-description">Nombre de sujets</div>
                                <div class="circle-tile-number transparent text-purple">'.$totalSubject.'</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="circle-tile">
                            <a>
                                <div class="circle-tile-heading purple">
                                    <i class="fa fa-comment fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content transparent text-purple">
                                <div class="circle-tile-description">Nombre de message</div>
                                <div class="circle-tile-number transparent text-purple">'.$totalMessage.'</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="circle-tile">
                            <a>
                                <div class="circle-tile-heading purple">
                                    <i class="fa fa-archive fa-3x" aria-hidden="true"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content transparent text-purple">
                                <div class="circle-tile-description">Nombre de jeton recolté</div>
                                <div class="circle-tile-number transparent text-purple">'.$totalJeton.'</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="circle-tile">
                            <a>
                                <div class="circle-tile-heading purple">
                                    <i class="fa fa-archive fa-3x" aria-hidden="true"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content transparent text-purple">
                                <div class="circle-tile-description">Nombre de vote</div>
                                <div class="circle-tile-number transparent text-purple">'.$totalVote.'</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="list-group">
                        <a href="stats.php" class="list-group-item list-group-item-info">Statistique général</a>
                        <a href="stats.php?option=bot" class="list-group-item list-group-item-info">Statistique robot</a>
                        <a href="stats.php?option=guest" class="list-group-item list-group-item-info">Statistique visiteurs</a>
                        <a href="stats.php?option=register" class="list-group-item list-group-item-info">Statistiques membres</a>
                        <a href="memberlist" class="list-group-item list-group-item-info">Liste des membres</a>
                        <a href="console" class="list-group-item list-group-item-info">Console</a>
                    </div>
                </div>
            </div>
        </section>';
    } else {
        include("permError.php");
    }
    include("..//footer.php"); ?>