<?php
    function bbCode($chaine)
    {    
        $chaine = str_replace("[b]", "<b>", $chaine);
        $chaine = str_replace("[/b]", "</b>", $chaine);

        $chaine = str_replace("[br/]", "<br/>", $chaine);
        $chaine = str_replace("[br]", "<br/>", $chaine);
    
        $chaine = str_replace("[i]", "<em>", $chaine);
        $chaine = str_replace("[/i]", "</em>", $chaine);
    
        $chaine = str_replace("[u]", "<u>", $chaine);
        $chaine = str_replace("[/u]", "</u>", $chaine);
    
    
        $chaine = str_replace("[code]", "<pre><code>", $chaine);
        $chaine = str_replace("[/code]", "</code></pre>", $chaine);            
            
        $chaine = ereg_replace("\[\*\]?([^\[]*) ?", "<li>\\1</li>", $chaine);    
        $chaine = str_replace(array('[list]','[/list]'), array('<ul>','</ul>'), $chaine);
    
        $chaine = preg_replace("#\[url\]((ht|f)tp://)([^\r\n\t<\"]*?)\[/url\]#sie", 
        "'<a href=\"\\1' . str_replace(' ', '%20', '\\3') . '\">\\1\\3</a>'", $chaine);
        $chaine = preg_replace("/\[url=(.+?)\](.+?)\[\/url\]/", 
        "<a href=\"$1\">$2</a>", $chaine);

        $chaine = ereg_replace("\[email\] ?([^\[]*) ?\[/email\]", 
        "<a href=\"mailto:\\1\">\\1</a>", $chaine);
        $chaine = ereg_replace("\[email ?=([^\[]*) ?] ?([^]]*) ?\[/email\]", 
        "<a href=\"mailto:\\1\">\\2</a>", $chaine);
    
        $chaine = ereg_replace("\[img\] ?([^\[]*) ?\[/img\]", 
        "<img width=\"100%\" src=\"\\1\" alt=\"\" />", $chaine);
        $chaine = ereg_replace("\[img ?= ?([^\[]*) ?\]", 
        "<img src=\"\\1\" alt=\"\" />", $chaine);
        return $chaine;
    }
    setlocale(LC_TIME, 'fr_FR.UTF-8');
    mysql_connect("localhost", "web", "octobre");
    mysql_select_db("MinewhiteV2");
    $id = $_GET['id'];
    $erreurText = "Erreur print message";
    $query = "SELECT * FROM forum_message WHERE sujet = '$id' ORDER BY id";
    $req = mysql_query($query) or print($erreurText);
    $name = $_GET['pseudo'];
    while ($donnees = mysql_fetch_array($req)) {
        $pseudo = $donnees['proprio'];
        $query2 = "SELECT * FROM forum_membre WHERE pseudo= '$pseudo'";
        $req2 = mysql_query($query2) or print('Erreur!');
        $donnees2 = mysql_fetch_array($req2);
        $date     = new DateTime($donnees['date']);
        $id = $donnees['id'];
        if (!isset($first)) {
            $pageDesc = 'Sujet ' . $sujet . ' de ' . ucfirst($proprio) . ' sur le forum de Minewhite - ' . ucfirst($donnees['content']) . '';
            $first    = true;
        }
        echo '<div class="col col-12">
            <div class="col">
                <hr>
            </div>
            <div class="col">
                <span class="fa-stack fa-3x fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="far fa-envelope fa-stack-1x fa-inverse"></i>
                </span>
            </div>
            <div class="col">
                <h3>' . $donnees['name'] . '
                    <small>
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                        <a style="color:#777" href="/forum/compte/' . $donnees2['id'] . '">
                            ' . ucfirst($donnees['proprio']) . '
                        </a>
                    </small>
                </h3>
                <p>
                    ' . str_replace(array("\r\n", "\n"), '<br>', bbCode($donnees['content'])) . '
                </p>';
                if (!empty($donnees['image'])) {
                    echo '<a class="btn btn-default" target="_blank" href="' . $donnees['image'] . '"><i class="fa fa-paperclip" aria-hidden="true"></i> Fichier</a>';
                }
                echo '<p align="right">';
                    if (isset($name)) {
                        if (($donnees['proprio'] == $name) || ($_GET['perm'] >= 5)) {
                            echo '<a href="/forum?action=edit&message=' . $id . '"><i class="fa fa-edit" aria-hidden="true"></i></a> ';
                            echo '<a onclick="deleteMessage('.$id.')"><i class="fa fa-times-circle" aria-hidden="true"></i></a><br>';
                        }
                    }
                    echo '<i>' . $date->format('j/m/y à H:i') . '</i>
                </p>
            </div>
        </div>';
    }