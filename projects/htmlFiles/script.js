function getHtmlPage(htmlFile) {
    if (htmlFile) {
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4) {
                if (this.status == 200) {return this.responseText;}
                if (this.status == 404) {return "Page not found.";}
                includeHTML();
            }
        }
        xhttp.open("GET", htmlFile, true);
        xhttp.send();
    }
}

function findNode() {
    let nodes = document.getElementsByClassName('include')
    nodes.forEach(node => {
        let content = getHtmlPage(node.innerHTML);
        node.parentNode.innerHTML = content;
        node.parentNode.removeChild(node);
    })
}

document.addEventListener("DOMContentLoaded", findNode);