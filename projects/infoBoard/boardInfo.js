const { app, BrowserWindow, ipcMain, globalShortcut } = require('electron')

let win;

function createWindow() {
	win = new BrowserWindow({
		width: 90,
		height: 60,
		icon: "img/globe.png",
		webPreferences: {
			nodeIntegration: true
		}
	})

	const ret = globalShortcut.register('CommandOrControl+X', () => {
		win.loadFile('html/index.html')
	  })

	win.removeMenu();
	win.maximize();
	win.setFullScreen(true);


	// and load the index.html of the app.
	win.loadFile('html/index.html')

	// Émit lorsque la fenêtre est fermée.
	win.on('closed', () => {
		win = null
	})
}

app.on('ready', createWindow)
app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit()
	}
})

app.on('activate', () => {
	if (win === null) {
		createWindow()
	}
})