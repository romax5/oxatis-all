const http = require('http');
const https = require('https');

function fetch(url, method = "GET", protocol = "http") {
    return new Promise((resolve, reject) => {
        if (protocol === "https")
            var prot = https;
        else
            var prot = http;
        prot.get(url, { "method": method }, (res) => {
            res.setEncoding('utf8');
            let rawData = '';
            res.on('data', (chunk) => { rawData += chunk; });
            res.on('end', () => {
                try {
                    resolve(JSON.parse(rawData));
                } catch (e) {
                    reject(e)
                }
            });
        }).on('error', (e) => {
            reject(e)
        });
    });
}

function getTimes(id) {
    return fetch(`http://map.rtm.fr/WebBusServeur/getStationDetails?nomPtReseau=${id}&response=application/json`)
}

function formatNumber(num){
  if ( num < 10 )
    return "0" + num;
  else
    return "" + num;
}

function clock() {
    var timer = document.getElementById("clock");
    let date = new Date();
    timer.innerHTML = date.getHours() + ":" + formatNumber(date.getMinutes());
}

async function getBusTime() {
    var busCards = document.getElementsByClassName("busStop");
    clock();
    for (let elem of busCards) {
        let line = elem.getAttribute('line');
        let lineInfo = await getTimes(line);
        lineInfo = lineInfo.getStationDetailsResponse;
        console.log(lineInfo);

        let cardHeader = document.createElement('div')
        cardHeader.classList = "card-header";
        cardHeader.innerHTML = `<i class="fas fa-map-marker-alt fa-fw"></i> ${lineInfo.comLieu}<br>`;

        let cardBody = document.createElement('div');
        cardBody.classList = "card-body text-center";
        cardBody.innerHTML = "";
        lineInfo.passage.forEach((elem, key) => {
            if (key > 3)
                return
            cardBody.innerHTML += `<h1 style="font-size:28px;"><span class="badge badge-secondary"><small><i class="fas fa-map-signs fa-fw"></i></small> ${elem.destination}</span>
                                <span class="badge badge-info">${elem.nomLigneCial}</span>
                                <span class="badge badge-success">${elem.heurePassageReel}</span></h1>`
        });
        elem.replaceChild(cardHeader, elem.firstElementChild);
        elem.replaceChild(cardBody, elem.lastElementChild);
    }
}

getBusTime();
setInterval(getBusTime, 10000)