function fetchUrl () {
	fetch('https://v3.minewhite.fr/GET/sections')
	.then(a => {
		a.json()
		.then(a => {
        	console.log(a);
        	a.forEach(a => {
                let body = document.getElementByTagName('body');
                let div = document.createElement('div');
                body.appendChild(div);
                div.innerHTML = '<div class="col col-md-8 col-12"><div class="col"><hr></div><a class="stretched-link" href="forum/section/'+ a.id +'"><div class="col"><i class="fa fa-comments fa-3x fa-pull-left fa-border text-secondary" aria-hidden="true"></i></div></a><div class="col forum-text"><h3>' + a.name + '</h3></div></div>';
            })
        })
	})
}

document.addEventListener("DOMContentLoaded", fetchUrl);